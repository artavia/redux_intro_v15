# Description
A tidier demo based on video tutorial series by Max Schwarz&#45;Mueller&rsquo;s introduction to ReactJS with Redux.

## Quick word about babel-presets
During compilation I was warned about **env** and to visit the [babel homepage](http://babeljs.io/env "link to env advisory") to read further on the subject. Although, I did not utilize **babel-preset-es2015** specifically, it is safe to say that **babel-preset-env** is going to be shelved in favor of **&#64;babel/preset-env**. You can see the changes in the commit history if you desire. Also, you may as well go for the gusto in this case. In the short run, you would run something to this effect to swap out one package for the other like so:
```sh
$ npm uninstall babel-preset-env babel-core babel-loader babel-preset-react transform-object-rest-spread --save-dev
$ npm install @babel/preset-env @babel/core "babel-loader@^8.0.0-beta" @babel/preset-react @babel/plugin-proposal-object-rest-spread --save-dev
```

## Please visit
You can see [the lesson](https://artavia.gitlab.io/redux_intro_v15/dist/index.html "the lesson at github") and decide if this is something for you to play around with at a later date. I have composed an accompanying repository that deals with [ReactJS version 16](https://www.github.com/donlucho/redux_intro_v16/ "link to companion repo"). 

## Some Additional Comments
Other than the final product, I have included the previous phases in the webpack.config.js file that went into comprehending the subject of Redux before tackling ReactJS with Redux, per se. At your leisure, you can either waste &lsquo;em or pore over them for your own benefit. If you are of a rare breed that does not need to warm&#45;up before any exercise, then, have at it! Also, rather than maintaining a standalone .babelrc JSON file, I included the additional parameters in the webpack.config.js file necessary to make this thing run, too.

## Main Sources
The original source I consulted can be found at Max&rsquo;s video series entitled [ReactJS + Redux Basics](https://www.youtube.com/playlist?list=PL55RiY5tL51rrC3sh8qLiYHqUV3twEYU_ "link to ReactJS + Redux Basics Series").

### My name is Luis
You can call me don Lucho &amp; I hope you have fun during the rest of your day building something cool to share with the rest of the world!