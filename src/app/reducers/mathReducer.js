
const initialMathState = { result: 1, lastValues: [] };
const mathReducer = (state = initialMathState, action) => { 
  switch (action.type){ 
    case "ADD": 
      state = { ...state, result: state.result + action.payload , lastValues: [ ...state.lastValues, action.payload ] };
      break;

    case "SUBTRACT":
    state = { ...state, result: state.result - action.payload , lastValues: [ ...state.lastValues, action.payload ] };
      break;
  }
  return state;  
};

export {mathReducer};