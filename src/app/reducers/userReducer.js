
const initialUserState = { username: "Lucho", age: 46 };
const userReducer = (state = initialUserState, action) => { 
  switch (action.type){ 
    
    // case "SET_NAME": 
    case "SET_NAME_FULFILLED": 
      state = { ...state , username: action.payload };
      break;

    case "SET_AGE":
      state = { ...state , age: action.payload };
      break;
  }
  return state;
};

export {userReducer};