function setName(name) {

  var mockAsync = (resolve, reject) => {
    if(Promise.resolve){
      console.log("resolved, baby");
      setTimeout( () => { resolve( name ); }, 1406 ); 
    }
    else 
    if(Promise.reject){
      console.log("rejected, chump");
      reject( (derrp) => {console.error( "rejected: ",  derrp ); } );
    }
  };

  var promise = new Promise( mockAsync );
  return {
    type: "SET_NAME" 
    , payload: promise 
  };

  // return (dispatch) => { setTimeout( () => { dispatch( {  type: "SET_NAME" , payload: name  } );  } , 1406 );  };
  
  // return {  type: "SET_NAME" , payload: name  };
}

function setAge(age) {
  return { type: "SET_AGE" , payload: age  };
}

export { setName, setAge };