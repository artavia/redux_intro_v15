function addNumber(number) {
  return {
    type: "ADD"
    , payload: number
  };
}

function subtractNumber(number) {
  return {
    type: "SUBTRACT"
    , payload: number
  };
}

export { addNumber, subtractNumber };