
import {createStore} from 'redux'; 

const initialAppState = { 
  result: 1, lastValues: [] , username: "Lucho"
};

const reducer = (state = initialAppState, action) => { 
  switch (action.type){ 

    case "ADD": 
      state = {
        ...state,
        result: state.result + action.payload
        , lastValues: [ ...state.lastValues, action.payload ]
      };
      break;

    case "SUBTRACT":
      state = {
        ...state,
        result: state.result - action.payload
        , lastValues: [ ...state.lastValues, action.payload ]
      };
      break;    
  }
  return state;  
};

// const store = createStore( reducer, initialAppState );
const store = createStore( reducer );

store.subscribe( () => {
  console.log( 'Store updated: ' , store.getState() ); 
} );

store.dispatch( { type: "ADD" , payload: 100 } ); // elicits 101
store.dispatch( { type: "ADD" , payload: 22 } ); // elicits 123
store.dispatch( { type: "SUBTRACT" , payload: 80 } ); // elicits 43 