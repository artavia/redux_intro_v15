import '../favicon.ico';
import '../css/boots.scss';
// import '../js/index.js';

import React from 'react';
import { connect } from 'react-redux';

import { User } from '../components/User';
import { Main } from '../components/Main';

import { setName } from '../actions/userActions';
import { setAge } from '../actions/userActions';
import { addNumber, subtractNumber } from '../actions/mathActions';

class CustomApp extends React.Component {
  
  // constructor(props){
    // super(props);
    // this.state = { username: "Lucho" };
    // this.changeUsername = this.changeUsername.bind(this);
  // }

  // changeUsername(newName){
    // this.setState( (prevState, props) => { if(prevState.username !== newName ){ return {username: newName }; } } );
  // }

  render(){
    return(
      <div className="container">
        {/*<Main changeUsername={ this.changeUsername }/>*/}        
        {/* <User username={this.state.username}/> */}
        {/* <User username={"Lucho"}/> */}        
        <Main changeUsername={ () => { this.props.setName("Roronoa Zoro") } }/>
        <User username={ this.props.user.username }/>
      </div>
    );
  }

}

const mapStateToProps = (state) => {
  return {
    user: state.userReducer
    , math: state.mathReducer
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setName: (username) => {
      // dispatch( { type: "SET_NAME" , payload: username } );
      dispatch( setName(username) );
    }
  };
};

// export {CustomApp};
export default connect( mapStateToProps, mapDispatchToProps)(CustomApp);