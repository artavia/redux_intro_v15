import '../favicon.ico';
import '../css/boots.scss';
// import '../js/index.js';

import React from 'react';
import { connect } from 'react-redux';

import { User } from './User';
import { Main } from './Main';

class CustomApp extends React.Component {
  
  render(){
    return(
      <div className="container">
        <Main changeUsername={ () => { this.props.setName("Roronoa Zoro") } }/>
        <User username={ this.props.user.username }/>
      </div>
    );
  }

}

const mapStateToProps = (state) => {
  return {
    user: state.userReducer
    , math: state.mathReducer
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setName: (username) => {
      dispatch( {
        type: "SET_NAME"
        , payload: username
      } );
    }
  };
};

// export {CustomApp};
export default connect( mapStateToProps, mapDispatchToProps)(CustomApp);