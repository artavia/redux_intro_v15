import React from 'react';

function User(props){
  return(
    <div>
      <div className="row">
        <div className="col-xs-12">
          <h1>User Component</h1>
        </div>
      </div>
      <div className="row">
        <div className="col-xs-12">
          <p>User Name: {props.username}</p>
        </div>
      </div>
    </div>
  );
}
export {User}; 

/* export class User extends React.Component{  
  render(){    
    var props = this.props;
    return(
      <div>
        <div className="row">
          <div className="col-xs-12">
            <h1>User Component</h1>
          </div>
        </div>
        <div className="row">
          <div className="col-xs-12">
            <p>User Name: {props.username}</p>
          </div>
        </div>
      </div>
    );
  }
} */

/* export const User = (props) => {
  return(
    <div>
      <div className="row">
        <div className="col-xs-12">
          <h1>User Component</h1>
        </div>
      </div>
      <div className="row">
        <div className="col-xs-12">
          <p>User Name: {props.username}</p>
        </div>
      </div>
    </div>
  );
}; */

