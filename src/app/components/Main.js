import React from 'react';

function Main(props) {
  return(
    <div>
      <div className="row">
        <div className="col-xs-12">
          <h1>Main Component</h1>
        </div>
      </div>
      <div className="row">
        <div className="col-xs-12">
        <button className="btn btn-primary" onClick={ () => props.changeUsername('Roronoa Zoro') }>Change Username</button>
        </div>
      </div>
    </div>
  );
}
export {Main}; 

/* export class Main extends React.Component{
  render(){    
    var props = this.props;
    return(
      <div>
        <div className="row">
          <div className="col-xs-12">
            <h1>Main Component</h1>
          </div>
        </div>
        <div className="row">
          <div className="col-xs-12">
          <button className="btn btn-primary" onClick={ () => props.changeUsername('Roronoa Zoro') }>Change Username</button>
          </div>
        </div>
      </div>
    );
  }
} */

/* export const Main = (props) => {
  return(
    <div>
      <div className="row">
        <div className="col-xs-12">
          <h1>Main Component</h1>
        </div>
      </div>
      <div className="row">
        <div className="col-xs-12">
        <button className="btn btn-primary" onClick={ () => props.changeUsername('Roronoa Zoro') }>Change Username</button>
        </div>
      </div>
    </div>
  );
}; */