
import {createStore} from 'redux'; 
import {combineReducers} from 'redux'; 

const initialMathState = { result: 1, lastValues: [] };

const mathReducer = (state = initialMathState, action) => { 
  switch (action.type){ 
    case "ADD": 
      state = { ...state, result: state.result + action.payload , lastValues: [ ...state.lastValues, action.payload ] };
      break;

    case "SUBTRACT":
    state = { ...state, result: state.result - action.payload , lastValues: [ ...state.lastValues, action.payload ] };
      break;
  }
  return state;  
};

const initialUserState = { username: "Lucho", age: 18 };

const userReducer = (state = initialUserState, action) => { 
  switch (action.type){ 
    case "SET_NAME": 
      state = { ...state , username: action.payload };
      break;

    case "SET_AGE":
      state = { ...state , age: action.payload };
      break;    
  }
  return state;
};

// const store = createStore( reducer );
// const store = createStore( combineReducers( {mathReducer, userReducer} ) ); // v1
const store = createStore( combineReducers( {mathReducer: mathReducer, userReducer: userReducer} ) ); // v2

const fatmethod = () => { console.log( 'Store updated: ' , store.getState() ); } ;
store.subscribe( fatmethod );

store.dispatch( { type: "ADD" , payload: 100 } ); 
store.dispatch( { type: "ADD" , payload: 22 } ); 
store.dispatch( { type: "SUBTRACT" , payload: 80 } ); 
store.dispatch( { type: "SET_NAME" , payload: 'Roronoa Zoro' } ); 
store.dispatch( { type: "SET_AGE" , payload: 23 } ); 