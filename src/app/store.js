import {createStore , combineReducers , applyMiddleware } from 'redux'; 
import logger from 'redux-logger';

import thunk from 'redux-thunk';
import promise from 'redux-promise-middleware';

import { mathReducer } from './reducers/mathReducer';
import { userReducer } from './reducers/userReducer';

const myLogger = (state) => (next) => (action) => {
  console.log("Logged action: " , action );
  next(action);
};

const store = createStore( combineReducers( 
  {mathReducer: mathReducer, userReducer: userReducer} ) 
  , {} 
  // , applyMiddleware( myLogger ) 
  // , applyMiddleware( logger() ) 

  // , applyMiddleware( logger(), thunk ) 
  , applyMiddleware( logger(), thunk, promise() ) 

);

// const fatmethod = () => { console.log( 'fatmethod ~ Store updated: ' , store.getState() );  } ;
// store.subscribe( () => { } ); // store.subscribe( fatmethod );

/* store.dispatch( { type: "ADD" , payload: 100 } ); 
store.dispatch( { type: "ADD" , payload: 22 } ); 
store.dispatch( { type: "SUBTRACT" , payload: 80 } ); 
store.dispatch( { type: "SET_NAME" , payload: 'Roronoa Zoro' } ); 
store.dispatch( { type: "SET_AGE" , payload: 23 } );  */

export { store };