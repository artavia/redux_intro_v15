
// ReactJS State Management can be handled with Redux. Why Redux? Redux has a different approach for dealing with state management than ReactJS and it all starts with a view (App in other words). 

import {createStore} from 'redux'; 

// createStore method is imported with the redux library. The createStore method accepts two arguments ~ the FIRST argument is the reducer to be employed, the part that takes your actions and changes your state as a consequence ~ the SECOND argument is the initial application state param.

// REDUCER ~ step 2 of 4 ~ A reducer has one simple task which is to take the action and handle the action by means of its [method]. A reducer will then take the old state and apply a change in place which now reflects the previous action which produces a new state. The reducer, therefore, PREFERABLY makes state immutable providing unique states as time progresses. 

// Reducer function accepts two parameters: first, the state to modify and return, then, the action object which has a label in ALL CAPS and a payload which contains a new value to latch on to or replace the old state

const initialAppState = 1;

const reducer = (state, action) => { 
  switch (action.type){ // determine which action occurred -- the new state is equal to the old state combined with the payload
      case "ADD":
        state = state + action.payload
        break;
      case "SUBTRACT":
      state = state - action.payload
        break;
  }
  return state; // new or unchanged state
};

// STORE - step 3 of 4 ~ The new state is then passed along to a [store]. The store's task is to store our state. There can be multiple reducers but there is normally only one store. 

const store = createStore( reducer, initialAppState );

// step 4 of 4 ~ Then, the application or component can [subscribe] (pass state in other words) to the store. So whenever the state is updated and added to the store, the store takes the new state and passes the new state along to all of the interested components and the application will update accordingly.

// In order to view changes, we need to call the subscribe method that accepts a function parameter which in this case returns the new state by means of calling the getState method

store.subscribe( () => {
  console.log( 'Store updated: ' , store.getState() ); 
} );

// ACTIONS ~ // step 1 of 4 ~ The application\view or any given component will [dispatch] [action]s. An action, then, is ran through a [reducer] (handle action, change and reduce state is its primary task). 

// Actions are triggered by the store. The reducer receives a dispatch method and it expects a pojo. The object has properties such as type (a label) and payload (a value). 

store.dispatch( { type: "ADD" , payload: 100 } ); // elicits 101
store.dispatch( { type: "ADD" , payload: 22 } ); // elicits 123
store.dispatch( { type: "SUBTRACT" , payload: 80 } ); // elicits 43 