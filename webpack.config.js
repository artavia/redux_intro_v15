"use strict";

const env = process.env.NODE_ENV;

const path = require("path");
const paths = {
  ROOT_DIR: path.resolve( __dirname ) // .
  , SRC_DIR: path.resolve( __dirname, 'src' ) // ./src
  , DIST_DIR: path.resolve( __dirname, 'dist' ) // ./dist
  , MODULES_DIR: path.resolve( __dirname, 'node_modules' ) // ./node_modules
};

const HtmlWebpackPlugin = require('html-webpack-plugin');
const hwp = new HtmlWebpackPlugin({
  filename: 'index.html'
  , template: paths.SRC_DIR + '/index.html'
});

const CleanWebpackPlugin = require('clean-webpack-plugin');
const cwp = new CleanWebpackPlugin( ['dist'] );

const ExtractTextPlugin = require('extract-text-webpack-plugin');
const extractSass = new ExtractTextPlugin({
  // filename: 'css/[name].css' // returns main.css
  filename: 'css/styles.css'
});

const webpack = require('webpack');
const hotModuleDev = new webpack.HotModuleReplacementPlugin({});
const wpp = new webpack.ProvidePlugin( {
  $: 'jquery'
  , jQuery: 'jquery'
} );
const nmp = new webpack.NamedModulesPlugin();

const woccp = new webpack.optimize.CommonsChunkPlugin( {
  name: 'dejavu'
} );

const config = {
  
  entry: {
    // main: paths.SRC_DIR + '/app/redux_zero.js' // video 3
    // main: paths.SRC_DIR + '/app/redux_one.js' // video 4
    // main: paths.SRC_DIR + '/app/redux_two.js' // video 5
    // main: paths.SRC_DIR + '/app/redux_three.js' // video 6
    // main: paths.SRC_DIR + '/app/redux_four.js' // video 7
    main: paths.SRC_DIR + '/app/app.js' // video 8
    , vendorcode: paths.SRC_DIR + '/app/vendorcode.js'
  }
  
  , devServer: {
    contentBase: paths.SRC_DIR
    , inline: true
    , historyApiFallback: true    
    , hot: true
    , host: '127.0.0.1'
    , port: 8080
    // , open: true
  } 

  , output: {
    filename: '[name].bundle.js'
    , path: paths.DIST_DIR 
    , publicPath: ''
  } 
  
  , devtool: "source-map"

  , module: {
    rules: [
      {
        test: /\.js$/ 
        , exclude: /(node_modules|bower_components)/
        , use: [
          {
            loader: 'babel-loader'
            , options: {
              // presets: ['react','env']
              presets: [ '@babel/preset-react' , '@babel/env' ]
              // , plugins: ['transform-object-rest-spread']
              , plugins: ['@babel/plugin-proposal-object-rest-spread']
            }
          }
        ]
      }
      , {
        test: /\.scss$/ 
        , use: extractSass.extract( {
          fallback: 'style-loader'
          , use: [ { loader: "css-loader", options: { alias: { "../fonts/bootstrap": "bootstrap-sass/assets/fonts/bootstrap" } } } 
          , { loader: "sass-loader", options: { includePaths: [ paths.MODULES_DIR + "/bootstrap-sass/assets/stylesheets" ] } }  ]
        } )
      }
      , {
        test: /\.woff2?$|\.ttf$|\.eot$|\.svg$/
        , use: [
          {
            loader: "file-loader"
            , options: {
              name: '[name].[ext]'
              , publicPath: '../fonts/' // , publicPath: '../'
              , outputPath: 'fonts/' 
            }
          }
        ]
      }
      , {
        test: /\.(ico)$/
        , use: [
          {
            loader: 'file-loader'
            , options: {
              name: '[name].[ext]'
              , outputPath: './'
            }
          }
        ]
      }
    ]
  }

  // v1
  , plugins: env === 'production' ? [cwp, nmp, hwp, woccp, extractSass, wpp] : [ cwp, nmp, hwp, woccp, extractSass, hotModuleDev, wpp]

};

module.exports = config;